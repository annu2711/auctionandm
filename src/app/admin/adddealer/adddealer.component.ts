import { Component, OnInit } from '@angular/core';
import { AdminServiceService } from '../../services/admin-service.service';
import { FormBuilder, FormGroup, Validators} from '@angular/forms';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-adddealer',
  templateUrl: './adddealer.component.html',
  styleUrls: ['./adddealer.component.css']
})
export class AdddealerComponent implements OnInit {
  dealerFormGroup: FormGroup;
  msg: any;
  dealers: any;

  constructor(
    private _adminservice: AdminServiceService,
    private _fb: FormBuilder,
    private toastr: ToastrService
  ) {
    this.submit_content();
   }

  ngOnInit() {
    this.dealerData();
  }

  submitDealer(dealerFormGroup)
  {
    console.log(this.dealerFormGroup.value);
    this._adminservice.submitDealer(this.dealerFormGroup.value).subscribe(
      (data:any) => {
        if(data.status === 200){
          this.toastr.success('Success', 'Dealer Addess Successfully');
          this.dealerData();
          dealerFormGroup.reset();
        }else{
          this.msg = data.msg;
        }
      },
      (error) => { console.log(error) }
    );
  }

  submit_content()
  {
    this.dealerFormGroup = this._fb.group({
      dealerName: ['', Validators.required],
      dealerMobile: ['', Validators.required],
      dealerEmail: ['', Validators.required],
      dealerPassword: ['', Validators.required],
      type: ['0']
    });
  }

  dealerData()
  {
    this._adminservice.getDealer().subscribe(
      (data:any) => {
        console.log(data);
        this.dealers = data.dealers;
      }
    );
  }


}
