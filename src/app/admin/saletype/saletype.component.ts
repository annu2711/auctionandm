import { Component, OnInit } from '@angular/core';
import { AdminServiceService } from '../../services/admin-service.service';
import { FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-saletype',
  templateUrl: './saletype.component.html',
  styleUrls: ['./saletype.component.css']
})
export class SaletypeComponent implements OnInit {
  texts:any;
  saleFormgroup:FormGroup;
  msg: any;
  faliure: any;
  num: number;
  constructor(
    private adminService: AdminServiceService,
    private fb: FormBuilder
    ) {

      this.saletypeDetail();

    }

  ngOnInit() {

    this.adminService.display().subscribe(
      (data: any) => {
        console.log('data', data);
      this.texts = data.data
    },
      (error) => { console.log(error); }
    );
    console.log("response ==>", this.texts);

  }

  saletypeDetail() {
    this.saleFormgroup = this.fb.group({
        saletype: ['', Validators.required]
    })
  }


  SubmitSaletype(saleFormgroup){
    console.log('submit=>', this.saleFormgroup.value);
    this.adminService.submitSale(saleFormgroup.value).subscribe(
      (data:any) => {
        console.log('data=>', data);
        if( data.status === 200 ){
          this.msg = 'Data Added Successfully';
        }else{
          this.msg = 'Data Added Not Successfully';
        }
      },
      (error: any) => { console.log(error); }
    );
  }

}
