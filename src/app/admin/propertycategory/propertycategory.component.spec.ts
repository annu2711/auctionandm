import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PropertycategoryComponent } from './propertycategory.component';

describe('PropertycategoryComponent', () => {
  let component: PropertycategoryComponent;
  let fixture: ComponentFixture<PropertycategoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PropertycategoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PropertycategoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
