import { Component, OnInit } from '@angular/core';
import { AdminServiceService } from '../../services/admin-service.service';
import { FormBuilder, FormGroup, Validators} from '@angular/forms';
import { MatTableDataSource } from '@angular/material/table';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-propertycategory',
  templateUrl: './propertycategory.component.html',
  styleUrls: ['./propertycategory.component.css']
})
export class PropertycategoryComponent implements OnInit {
  salecategorys: any;
  catFormgroup: FormGroup;

  displayedColumns = ['sno', 'sale_type', 'cat_name', 'cat_key'];
  dataSource: MatTableDataSource < PeriodicElement[] > ;

  constructor(
    private _adminservice: AdminServiceService,
    private _fb: FormBuilder,
    private toastr: ToastrService
  ) {

  }

  ngOnInit() {
    this.saleoptions();
    this.submit_content();
    this.categoryList();
  }

  saleoptions() {
    this._adminservice.saleOpt().subscribe(
      (data:any) => {
        if(data.status === 200){
          this.salecategorys = data.data;
        }
      }
    )
  }

  categoryList(){
    this._adminservice.propertycategorylist().subscribe(
      (data:any) => {
        console.log('category list =>', data);
        this.dataSource = data.PropertyCategory;
      },
      (error) => { console.log(error) }
    )
  }

  submitCategory(catFormgroup){
    console.log("category =>", this.catFormgroup.value);
    this._adminservice.addPropertyCat(this.catFormgroup.value).subscribe(
      (data: any) => {
        console.log("submit Response =>", data);
        this.catFormgroup.reset();
        this.toastr.success('Success', 'Property Category Added Successfully');
        this.categoryList();
      },
      (error) => { console.log(error); }
    )
  }

  submit_content(){
      this.catFormgroup = this._fb.group({
      saletype: ['', Validators.required],
      propertycat: ['', Validators.required]
    });
  }
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

}

export interface PeriodicElement {
  sno: number;
  sale_type: string;
  cat_name: string;
  cat_key: string;
}
