import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import { SaletypeComponent } from './saletype/saletype.component';
import { LoginComponent } from './login/login.component';
import { AddpropertyComponent } from './addproperty/addproperty.component';
import { PropertysubcatComponent } from './propertysubcat/propertysubcat.component';
import { AdddealerComponent } from './adddealer/adddealer.component';
import { PropertylistComponent } from './propertylist/propertylist.component';
import { DealerbannerComponent } from './dealerbanner/dealerbanner.component';
import { PropertycategoryComponent } from './propertycategory/propertycategory.component';
import { PossessionComponent } from './possession/possession.component';


const routes: Routes = [
 {path: '', component: LoginComponent},
 {path: 'dashboard', component: DashboardComponent},
 {path: 'saletype', component: SaletypeComponent},
 {path: 'addproperty', component: AddpropertyComponent},
 {path: 'addpropertysubcategory', component: PropertysubcatComponent},
 {path: 'dealer', component: AdddealerComponent},
 {path: 'propertylist', component: PropertylistComponent},
 {path: 'addbanner', component: DealerbannerComponent},
 {path: 'propertycategory', component: PropertycategoryComponent},
 {path: 'possession', component: PossessionComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
