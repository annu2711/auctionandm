import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './dashboard/dashboard.component';
import { AdminRoutingModule } from './admin-routing.module';
import { SaletypeComponent } from './saletype/saletype.component';
import { LoginComponent } from './login/login.component';
import { ReactiveFormsModule } from '@angular/forms';
import { SideMenuComponent } from './side-menu/side-menu.component';
import { HeaderComponent } from './header/header.component';
import { AddpropertyComponent } from './addproperty/addproperty.component';
import { PropertysubcatComponent } from './propertysubcat/propertysubcat.component';
import { AdddealerComponent } from './adddealer/adddealer.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { PropertylistComponent } from './propertylist/propertylist.component';
import {MatButtonModule} from '@angular/material/button';
import {MatTableModule} from '@angular/material/table';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatMenuModule} from '@angular/material/menu';
import { DealerbannerComponent } from './dealerbanner/dealerbanner.component';
import { PropertycategoryComponent } from './propertycategory/propertycategory.component';
import { MatSelectModule } from '@angular/material/select';
import { PossessionComponent } from './possession/possession.component';



@NgModule({
  declarations: [
    DashboardComponent,
    SaletypeComponent,
    LoginComponent,
    SideMenuComponent,
    HeaderComponent,
    AddpropertyComponent,
    PropertysubcatComponent,
    AdddealerComponent,
    PropertylistComponent,
    DealerbannerComponent,
    PropertycategoryComponent,
    PossessionComponent,
    

  ],
  imports: [
    CommonModule,
    AdminRoutingModule,
    ReactiveFormsModule,
    MatButtonModule,
    NgbModule,
    MatTableModule,
    MatFormFieldModule,
    MatInputModule,
    MatPaginatorModule,
    MatMenuModule,
    MatSelectModule

  ]
})
export class AdminModule { }
