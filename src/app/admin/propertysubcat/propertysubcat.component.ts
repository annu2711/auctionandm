import { Component, OnInit } from '@angular/core';
import { AdminServiceService } from '../../services/admin-service.service';
import { FormBuilder, FormGroup, Validators} from '@angular/forms';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-propertysubcat',
  templateUrl: './propertysubcat.component.html',
  styleUrls: ['./propertysubcat.component.css']
})
export class PropertysubcatComponent implements OnInit {
  subcatFormgroup: FormGroup;
  salecategorys: any;
  propcategorys: any;
  subcattable: any;
  msg: any;

  constructor(
    private _adminservice: AdminServiceService,
    private _fb: FormBuilder,
    private toastr: ToastrService
  ) {
    this.submit_content();
  }

  ngOnInit()
  {
    this.saleoptions();
    this.subcategoryTable();

  }

  SubmitPropsubcat(subcatFormgroup){
    console.log(this.subcatFormgroup.value);
    this._adminservice.submitPropsubcat(this.subcatFormgroup.value).subscribe(
      (data:any) => {
       // console.log("submit data =>", data);

        this.toastr.success('Success', 'Form Submit Successfully');
        this.subcatFormgroup.reset();
        this.subcategoryTable();
       // this.msg = '';
      },
      (error) => { console.log(error) }
    );
  }

  proptype(data)
  {
    console.log(data);
    this._adminservice.propcatOption({'saletype': data}).subscribe(
      (data:any) => {
        //console.log('prop=>', data);
        if(data.status === 200){
          this.propcategorys = data.PropertyOption;
        }
      },
      (error) => { console.log(error) }
    );
  }

  submit_content(){
    this.subcatFormgroup = this._fb.group({
      saletype: ['', Validators.required],
      propertytype: ['', Validators.required],
      propsubcat: ['', Validators.required]
    });
  }

  saleoptions() {
    this._adminservice.saleOpt().subscribe(
      (data:any) => {
        if(data.status === 200){
          this.salecategorys = data.data;
        }
      }
    )
  }

  subcategoryTable(){
    this._adminservice.subcattable().subscribe(
      (data:any) => {
      //  console.log("tabel => ", data);
        if(data.status === 200){
          this.subcattable = data.records;
        }
      },
      (error) => { console.log(error) }
    );
  }

}
