import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PropertysubcatComponent } from './propertysubcat.component';

describe('PropertysubcatComponent', () => {
  let component: PropertysubcatComponent;
  let fixture: ComponentFixture<PropertysubcatComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PropertysubcatComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PropertysubcatComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
