import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder } from '@angular/forms';
import { AdminServiceService } from '../../services/admin-service.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  invalid: any;

  constructor(
    private _adminservice: AdminServiceService,
     private _router: Router,
     private frmbuilder: FormBuilder,
  ) {
    this.x();
   }

  ngOnInit() {
  }

  x() {
    this.loginForm = this.frmbuilder.group({
      username: new FormControl(),
      password: new FormControl()
  });
  }

  Login (loginForm) {
    console.log(this.loginForm.value);
    this._adminservice.login(loginForm.value).subscribe(

      (data:any) => {
        console.log('response=>', data);
        if(data.status === 200){
          localStorage.setItem('token', data.Token);
          localStorage.setItem('user', data.Key);
          this._router.navigate(['admin/dashboard']);
        } else {
          this.invalid = 'Invalid Credentials';
        }
      },
      (error) => { console.log('no response => ', error); }

    );
  }
}
