import { Component, OnInit } from '@angular/core';
import { AdminServiceService } from '../../services/admin-service.service';
import { FormBuilder, FormGroup, Validators} from '@angular/forms';
import { MatTableDataSource } from '@angular/material/table';
import { ToastrService } from 'ngx-toastr';


@Component({
  selector: 'app-dealerbanner',
  templateUrl: './dealerbanner.component.html',
  styleUrls: ['./dealerbanner.component.css']
})
export class DealerbannerComponent implements OnInit {
  dealerFormGroup: FormGroup;
  dealers: any;
  url: any;
  photo: any;
  imgname: any;
  name: any;
  title: any;

  displayedColumns = ['sno', 'c_name', 'title', 'img', 'db_key'];
  dataSource: MatTableDataSource < PeriodicElement[] > ;

  constructor(
    private _adminservice: AdminServiceService,
    private _fb: FormBuilder,
    private _toast: ToastrService
  ) { }

  ngOnInit() {
    this.submitContent();
    this.dealerOpt();
    this.bannersList();
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  dealerOpt()
  {
    this._adminservice.getDealeropt().subscribe(
      (data:any) => {
        console.log('response => ', data);
        this.dealers = data.dealers;
      },
      (error) => { console.log(error) }
    );
  }

  bannert(data){
    this.title = data;
  }

  dealerN(data){
    this.name = data;
  }

  onSelectFile(event:any) {
    if (event.target.files && event.target.files[0]) {
        var reader = new FileReader();
        reader.readAsDataURL(event.target.files[0]);
      this.photo = <File>event.target.files[0];
      console.log(this.photo);
      reader.onload = (event:any) => {
      this.url = reader.result;
      }
    }
  }

  submitBanner(dealerFormGroup){
    console.log(this.dealerFormGroup.value);
    this._adminservice.saveImage(this.photo).subscribe(
      (data:any) => {
        console.log("response => ", data);
        if(data.status === 200){
        this.imgname = data.msg;
         this.saveBan(this.imgname);
        }else{
          console.log('error =>', data);
        }
      },
      (error) => { console.log(error) }
    )
  }

  submitContent()
  {
    this.dealerFormGroup = this._fb.group({
      dealerName: ['', Validators.required],
      photo: ['', Validators.required],
      bannertitle: ['', Validators.required]
    })
  }

  saveBan(data){
    this._adminservice.saveDealerBanner({ "dealer": this.name, "image": data, "title": this.title }).subscribe(
      (data:any) => {
        console.log('Final =>', data);
        this._toast.success('Success', 'Dealer Banner Added Successfully');
        this.dealerFormGroup.reset();
        this.url = "";
      },
      (error) => { console.log(error) }
    )
  }


  bannersList(){
    this._adminservice.bannerlist().subscribe(
      (data:any) => {
        console.log('data => ', data);
        this.dataSource = new MatTableDataSource(data.Banners);
        console.log('response => ', this.dataSource);
      }
    )
  }


  deleteIt(data){
    console.log(data);
    this._adminservice.bannerDelete(data).subscribe(
      (data:any) => {
        console.log('data => ', data);
        this._toast.success('Success', 'Dealer Banner Deleted Successfully');
        this.bannersList();
      },
      (error) => { console.log(error) }
    )
  }



}


export interface PeriodicElement {
  sno: number;
  c_name: string;
  title: string;
  img: string;
  db_key: string;
}
