import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DealerbannerComponent } from './dealerbanner.component';

describe('DealerbannerComponent', () => {
  let component: DealerbannerComponent;
  let fixture: ComponentFixture<DealerbannerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DealerbannerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DealerbannerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
