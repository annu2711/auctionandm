import { Component, OnInit } from '@angular/core';
import { AdminServiceService } from '../../services/admin-service.service';
import { FormBuilder, FormGroup, Validators} from '@angular/forms';
import { MatTableDataSource } from '@angular/material/table';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-possession',
  templateUrl: './possession.component.html',
  styleUrls: ['./possession.component.css']
})
export class PossessionComponent implements OnInit {
  possessionFormgroup: FormGroup;
  submitted = false;
  possessions: any;
  displayedColumns = ['sno', 'possession', 'pos_key'];
  dataSource: MatTableDataSource < PeriodicElement[] > ;

  constructor(
    private _adminservice: AdminServiceService,
    private _fb: FormBuilder,
    private _toastr: ToastrService,
  ) { }

  ngOnInit() {
    this.submitContent();
    this.possessionTable();
  }

  SubmitPossession(possessionFormgroup: any){
    console.log('submit data =>', this.possessionFormgroup.value);
    this.submitted = true;
    this._adminservice.submitpossession(this.possessionFormgroup.value).subscribe(
      (data:any) => {
        console.log('response => ', data);
        this.possessionFormgroup.reset();
        this._toastr.success('Success', 'Property Possession Added Successfully');
        this.possessionTable();
   },
   (error) => { console.log(error) }
    );
  }

  submitContent()
  {
    this.possessionFormgroup = this._fb.group({
      possession: ['', Validators.required]
    });
  }

  possessionTable()
  {
    this._adminservice.getPossession().subscribe(
      (data: any) => {
        console.log('DATA LIST =>', data);
        this.dataSource = data.Possessions;

      }
    )
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

}


export interface PeriodicElement {
  sno: number;
  possession: string;
  pos_key: string;
}