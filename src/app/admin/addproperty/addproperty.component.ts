import { Component, OnInit } from '@angular/core';
import { DataTablesModule } from 'angular-datatables';
import { AdminServiceService } from '../../services/admin-service.service';
import { FormBuilder, FormGroup, Validators} from '@angular/forms';
import {NgbDateStruct, NgbCalendar} from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-addproperty',
  templateUrl: './addproperty.component.html',
  styleUrls: ['./addproperty.component.css']
})
export class AddpropertyComponent implements OnInit {
  model: NgbDateStruct;
  date: {year: number, month: number};
  addpropFormgroup: FormGroup;
  msg:any;
  salecategorys: any;
  propcategorys: any;
  subcategorys: any;
  states: any;
  cities: any;
  submitted = false;
  possessions: any;



  constructor(
    private _adminservice: AdminServiceService,
    private _fb: FormBuilder,
    private toastr: ToastrService
  ) {
    this.submitContent();
   }

  ngOnInit() {
    this.saletypedata();
    this.stateData();
    this.possessionTable();
  }



  submitProperty(addpropFormgroup)
  {
    console.log("Data=>", this.addpropFormgroup);
    this.submitted = true;
    if(this.addpropFormgroup.status === 'VALID'){
    this._adminservice.addProperty(this.addpropFormgroup.value).subscribe(
      (data:any) => {
        console.log('Submit Response => ', data);
        if(data.status === 200){
          this.toastr.success('Success', 'Property Submit Successfully');
          this.submitted = false;
          this.addpropFormgroup.reset();
        }
      },
      (error) => { console.log(error) }
    );
  }
  }

  proptype(data)
  {
    console.log(data);
    this._adminservice.propcatOption({'saletype': data}).subscribe(
      (data:any) => {
      //  console.log('prop=>', data);
        if(data.status === 200){
          this.propcategorys = data.PropertyOption;
        }
      },
      (error) => { console.log(error) }
    );
  }

  propcate(data)
  {
    console.log(data);
    this._adminservice.propsubcatOpt({'propcat': data}).subscribe(
      (data:any) => {
     //   console.log('subcat => ', data);
        this.subcategorys = data.Options;
      }
    )
  }


  saletypedata()
  {
    this._adminservice.saleOpt().subscribe(
      (data:any) => {
    //    console.log("sale=>", data);
        if(data.status === 200){
          this.salecategorys = data.data;
        }
      }
    )
  }

  mystate(data)
  {
    console.log(data);

    this._adminservice.getCity({ 'state' : data }).subscribe(
      (data:any) => {
        this.cities = data.Cities;
      },
      (error) => { console.log(error); }
    )
  }


   stateData()
   {
    this._adminservice.getState().subscribe(
      (data:any) => {
        this.states = data.states;
      },
      (error) => { console.log(error) }
    );
   }

   submitContent()
   {
    this.addpropFormgroup = this._fb.group({
      saletype: ['', Validators.required],
      propcategory: ['', Validators.required],
      propertysubcat: ['', Validators.required],
      propTitle: ['', Validators.required],
      auctionDate: ['', Validators.required],
      bidDate: ['', Validators.required],
      status: ['', Validators.required],
      contact: ['', Validators.required],
      contactPerson: ['', Validators.required],
      state: ['', Validators.required],
      city: ['', Validators.required],
      location: ['', Validators.required],
      address: ['', Validators.required],
      area: ['', Validators.required],
      areaParam: ['', Validators.required],
      remark: ['', Validators.required],
      price: ['', Validators.required],
      bankbranch: ['', Validators.required],
      bank: ['', Validators.required],
      possession: ['', Validators.required]
    });
   }

   possessionTable()
   {
     this._adminservice.getPossession().subscribe(
       (data: any) => {
         console.log('DATA LIST =>', data);
         this.possessions = data.Possessions;
 
       }
     )
   }

}
