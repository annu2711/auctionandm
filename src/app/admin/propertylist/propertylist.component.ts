import { Component, OnInit, ViewChild } from '@angular/core';
// import { MatPaginator } from '@angular/material/paginator';
import { AdminServiceService } from '../../services/admin-service.service';
import { MatTableDataSource } from '@angular/material/table';
import { FormBuilder, FormGroup, Validators} from '@angular/forms';
import { ToastrService } from 'ngx-toastr';




@Component({
  selector: 'app-propertylist',
  templateUrl: './propertylist.component.html',
  styleUrls: ['./propertylist.component.css']
})



export class PropertylistComponent implements OnInit {
  // 'sale_type', 'cat_name', 'subcat_name', 'area', 'auctiondate', 'biddate', 'status', 'con_person', 'contact'
  states: any;
  salecategorys: any;
  propcategorys: any;
  subcategorys: any;
  submitted = false;
  updatepropFormgroup: FormGroup;
  propertydata: any;
  mymodal = false;
  cities: any;
  selectsaletype: any;
  selectproptype: any;
  selectpropcat: any;
  selecstate: any;
  selectcity: any;
  selectstatus: any;
  rd = true;
  displayedColumns = ['sno', 'title', 'price', 'cat_name', 'subcat_name', 'area', 'auctiondate', 'biddate', 'status', 'address', 'p_key'];
  dataSource: MatTableDataSource < PeriodicElement[] > ;
  status = {0:'Upcoming',1:'Failed',2:'Live'};
  possessions: any;

  // @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;


  constructor(
      private _adminservice: AdminServiceService,
      private _fb: FormBuilder,
      private toastr: ToastrService
  ) { }



  ngOnInit() {
    // this.dataSource.paginator = this.paginator;

    this._adminservice.getAuctiondata().subscribe(
      (data:any) => {
        console.log('data => ', data);
        this.dataSource = new MatTableDataSource(data.Properties);
        console.log('response => ', this.dataSource);
      }
    );
    this.saletypedata();
    this.stateData();
    this.submitContent();
    this.possessionTable();
  }



  saletypedata()
  {
    this._adminservice.saleOpt().subscribe(
      (data:any) => {
    //    console.log("sale=>", data);
        if(data.status === 200){
          this.salecategorys = data.data;
        }
      }
    )
  }

  stateData()
  {
   this._adminservice.getState().subscribe(
     (data:any) => {
       this.states = data.states;
     },
     (error) => { console.log(error) }
   );
  }

  mystate(data: any)
  {
    console.log(data);

    this._adminservice.getCity({ 'state' : data }).subscribe(
      (data:any) => {
        this.cities = data.Cities;
      },
      (error) => { console.log(error); }
    )
  }

  proptype(data: any)
  {
    console.log(data);
    this._adminservice.propcatOption({'saletype': data}).subscribe(
      (data:any) => {
      //  console.log('prop=>', data);
        if(data.status === 200){
          this.propcategorys = data.PropertyOption;
          
        }
      },
      (error) => { console.log(error) }
    );
  }

  propcate(data: any)
  {
    console.log(data);
    this._adminservice.propsubcatOpt({'propcat': data}).subscribe(
      (data:any) => {
     //   console.log('subcat => ', data);
        this.subcategorys = data.Options;
      }
    )
  }

  submitContent()
  {
   this.updatepropFormgroup = this._fb.group({
     saletype: ['', Validators.required],
     propcategory: ['', Validators.required],
     propertysubcat: ['', Validators.required],
     propTitle: ['', Validators.required],
     auctionDate: ['', Validators.required],
     bidDate: ['', Validators.required],
     status: ['', Validators.required],
     contact: ['', Validators.required],
     contactPerson: ['', Validators.required],
     state: ['', Validators.required],
     city: ['', Validators.required],
     location: ['', Validators.required],
     address: ['', Validators.required],
     area: ['', Validators.required],
     areaParam: ['', Validators.required],
     remark: ['', Validators.required],
     price: ['', Validators.required],
     possession: ['', Validators.required],
     bank: ['', Validators.required],
     bankbranch: ['', Validators.required]
   });
  }


  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  getStatus(statusKey){
   return this.status[statusKey];
  }

  getView(data: any){
    this._adminservice.propFullview({'property':data}).subscribe(
      (data:any) => {
        console.log("fullview =>", data);
        this.propertydata = data.Property;
        this.mymodal = true;
        this.proptype(this.propertydata.sale_type);
        this.propcate(this.propertydata.prop_type);
        this.mystate(this.propertydata.state);
        this.selecttypeData(data.Property);

      },
      (error) => { console.log(error) }
    );
  }

  closeThis()
  {
    this.mymodal = false;
  }


  submitProperty(updatepropFormgroup)
  {
    console.log("Data=>", this.updatepropFormgroup);
    this.submitted = true;
    if(this.updatepropFormgroup.status === 'VALID'){
    this._adminservice.addProperty(this.updatepropFormgroup.value).subscribe(
      (data:any) => {
        console.log('Submit Response => ', data);
        if(data.status === 200){
          this.toastr.success('Success', 'Property Submit Successfully');
          this.submitted = false;
          this.updatepropFormgroup.reset();
        }
      },
      (error) => { console.log(error) }
    );
  }
  }

  //select option data
  selectThis(x: any, y: any){

  }

  selecttypeData(pdata: any){
    let areas = pdata.area.split('_');
    this.updatepropFormgroup.patchValue({
      area: areas[0],
      areaParam: areas[1],
      state: pdata.state,
      saletype: pdata.sale_type,
      propcategory: pdata.prop_type,
      propertysubcat: pdata.prop_cat,
      city: pdata.city,
      location: pdata.location,
      address: pdata.address,
      price: pdata.price,
      status: pdata.status,
      title: pdata.title,
      auctionDate: pdata.auctiondate,
      bidDate: pdata.biddate,
      remark: pdata.remark,
      contactPerson: pdata.con_person,
      contact: pdata.contact,
      possession: pdata.possession,
      bank: pdata.bank,
      bankbranch: pdata.bank_branch,
    });
  }

  possessionTable()
  {
    this._adminservice.getPossession().subscribe(
      (data: any) => {
        console.log('DATA LIST =>', data);
        this.possessions = data.Possessions;

      }
    )
  }

}

export interface PeriodicElement {
  sno: number;
  title: number;
  price: number;
  // sale_type: string;
  cat_name: string;
  subcat_name: string;
  area: string;
  auctiondate: string;
  biddate: string;
  status: string;
  // con_person: string;
  // contact: string;
  address: string;
  p_key: string;

}




