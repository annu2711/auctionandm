import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UrlService {

  constructor() { }

  public get APP_URL(): string {
    return environment.host;
  }

  public get LOGIN_ADMIN(): string {
    return `${this.APP_URL}/admin/login` ;
}

  public get SALETYPE_DISPLAY(): string {
    return `${this.APP_URL}/admin/propertySaletype`;
  }

  public get SUBMIT_SALE(): string {
    return `${this.APP_URL}/admin/propertySaletype`;
  }

  public get SALE_OPTION(): string {
    return `${this.APP_URL}/admin/propertySaletype`;
  }

  public get PROPCAT_OPTION(): string {
    return `${this.APP_URL}/PropertyOpt`;
  }

  public get PROPSUBCAT_OPTION(): string {
    return `${this.APP_URL}/PropertysubOpt`;
  }

  public get PROPSUBCAT_SUBMIT(): string {
    return `${this.APP_URL}/admin/subcategory`;
  }

  public get SUBCAT_TABLE(): string {
    return `${this.APP_URL}/admin/subcategory`;
  }

  public get SUBMIT_DEALER(): string {
    return `${this.APP_URL}/signup`;
  }

  public get DEALER_TABLE(): string {
    return `${this.APP_URL}/admin/dealerData`;
  }

  public get STATE(): string {
    return `${this.APP_URL}/state`;
  }

  public get CITY(): string {
    return `${this.APP_URL}/city`;
  }

  public get ADD_PROPERTY(): string {
    return `${this.APP_URL}/admin/addproperty`;
  }

  public get AUCTION_LIST(): string {
    return `${this.APP_URL}/admin/auctionlist`;
  }

  public get DEALER_OPTION(): string {
    return `${this.APP_URL}/admin/userOpt`;
  }

  public get IMAGE_UPLOAD(): string {
    return `${this.APP_URL}/imageupload`;
  }

  public get SAVE_BANNER(): string {
    return `${this.APP_URL}/admin/savebanner`;
  }

  public get BANNER_LIST(): string {
    return  `${this.APP_URL}/admin/dealerbanner`;
  }

  public BANNER_DELETE(data): string {
    return `${this.APP_URL}/admin/deletebanner/${data}`;
  }

  public get PROPCAT_LIST(): string {
    return `${this.APP_URL}/admin/propcatlist`;
  }

  public get ADD_PROPERTY_CAT(): string {
    return `${this.APP_URL}/admin/addcategory`;
  }

  public get FULL_PROP_VIEW(): string {
    return `${this.APP_URL}/admin/propertyview`;
  }

  public get ADD_POSSESSION(): string {
    return `${this.APP_URL}/admin/possession`;
  }

  public get POSSESSION_LIST(): string {
    return `${this.APP_URL}/admin/possessionlist`;
  }


  
}
