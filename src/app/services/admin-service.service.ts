import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { UrlService } from 'src/app/constants/url.service';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AdminServiceService {

  // url: 'http://127.0.0.1:8000/api/admin/';

  constructor(
    private http: HttpClient,
    private route: Router,
    private url: UrlService
  ) { }

  login(data) {
    return this.http.post(this.url.LOGIN_ADMIN, data);
  }

  logout(){
    localStorage.clear();
    this.route.navigate(['admin']);
  }

  display() {
    return this.http.get(this.url.SALETYPE_DISPLAY);
  }

  submitSale(data: any) {
    return this.http.post(this.url.SUBMIT_SALE, data);
  }

  saleOpt(){
    return this.http.get(this.url.SALE_OPTION);
  }

  propcatOption(data)
  {
    return this.http.post(this.url.PROPCAT_OPTION, data);
  }

  propsubcatOpt(data)
  {
    return this.http.post(this.url.PROPSUBCAT_OPTION, data);
  }

  submitPropsubcat(data)
  {
    return this.http.post(this.url.PROPSUBCAT_SUBMIT, data);
  }

  subcattable(){
    return this.http.get(this.url.SUBCAT_TABLE);
  }

  submitDealer(data){
    return this.http.post(this.url.SUBMIT_DEALER, data);
  }

  getDealer(){
    return this.http.get(this.url.DEALER_TABLE);
  }

  getState(){
    return this.http.get(this.url.STATE);
  }

  getCity(data: any)
  {
    return this.http.post(this.url.CITY, data);
  }

  addProperty(data: any)
  {
    return this.http.post(this.url.ADD_PROPERTY, data);
  }

  getAuctiondata()
  {
    return this.http.get(this.url.AUCTION_LIST);
  }

  getDealeropt(){
    return this.http.get(this.url.DEALER_OPTION);
  }

  saveImage(data: any){
    let formData = new FormData();
    formData.append('photo', data);
    return this.http.post(this.url.IMAGE_UPLOAD, formData);
  }

  saveDealerBanner(data: any){
    return this.http.post(this.url.SAVE_BANNER, data);
  }

  bannerlist(){
    return this.http.get(this.url.BANNER_LIST);
  }

  bannerDelete(data: any){
    return this.http.delete(this.url.BANNER_DELETE(data));
  }

  propertycategorylist(){
    return this.http.get(this.url.PROPCAT_LIST);
  }

  addPropertyCat(data: any){
    return this.http.post(this.url.ADD_PROPERTY_CAT, data);
  }

  propFullview(data: any){
    return this.http.post(this.url.FULL_PROP_VIEW, data);
  }

  submitpossession(data: any){
    return this.http.post(this.url.ADD_POSSESSION, data);
  }

  getPossession()
  {
    return this.http.get(this.url.POSSESSION_LIST);
  }
  
}
