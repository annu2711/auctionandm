import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpEvent, HttpHandler, HttpRequest, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AdminServiceService } from '../services/admin-service.service';

@Injectable()
export class BasicInterceptorService implements HttpInterceptor {

  constructor(
    private _adminservice: AdminServiceService,
  ) {

  }

  intercept (req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    if(localStorage.getItem('token') && localStorage.getItem('user')){
    let token = localStorage.getItem('token');
    let key = localStorage.getItem('user');

    if(token!= "" && key!= ""){
      // console.log('Hii');
      const authReq = req.clone({
        headers: new HttpHeaders({
         'Token': token,
         'Key': key
        })
      });
      // console.log('Intercepted HTTP call', authReq);
      return next.handle(authReq);

      } else {
        // console.log('no1 notoken');
        this._adminservice.logout();
        }
      }
      else{
        // console.log('no2 notoken');
        this._adminservice.logout();
        const authReq = req.clone({
          headers: new HttpHeaders({

          })
        });
        return next.handle(authReq);
      }

}


}
