import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DataTablesModule } from 'angular-datatables';
import { NgModule } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BasicInterceptorService } from './interceptor/basic-interceptor.service';
import { AdminServiceService } from './services/admin-service.service';
import { ToastrModule } from 'ngx-toastr';
import {MatButtonModule} from '@angular/material/button';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule, // required animations module
    DataTablesModule,
    HttpClientModule,
    AppRoutingModule,
    MatButtonModule,
    ToastrModule.forRoot() // ToastrModule added
  ],
  providers: [
    {
    provide: HTTP_INTERCEPTORS,
    useClass: BasicInterceptorService,
    multi: true
  }
],
  bootstrap: [AppComponent]
})
export class AppModule { }
